#include "src/TrigMufastHypoAlg.h"
#include "src/TrigMufastHypoTool.h"
#include "src/TrigmuCombHypoAlg.h"
#include "src/TrigmuCombHypoTool.h"
#include "src/TrigMuisoHypoAlg.h"
#include "src/TrigMuonEFTrackIsolationHypoAlg.h"
#include "src/TrigMuonEFTrackIsolationHypoTool.h"
#include "src/TrigMuonEFHypoAlg.h"
#include "src/TrigMuonEFHypoTool.h"
#include "src/TrigMuonEFInvMassHypoAlg.h"
#include "src/TrigMuonEFInvMassHypoTool.h"
#include "src/TrigL2MuonOverlapRemoverMufastAlg.h"
#include "src/TrigL2MuonOverlapRemoverMucombAlg.h"
#include "src/TrigL2MuonOverlapRemoverTool.h"
#include "src/TrigMuonLateMuRoIHypoAlg.h"
#include "src/TrigMuonLateMuRoIHypoTool.h"


DECLARE_COMPONENT( TrigMufastHypoAlg )
DECLARE_COMPONENT( TrigMufastHypoTool )
DECLARE_COMPONENT( TrigmuCombHypoAlg )
DECLARE_COMPONENT( TrigmuCombHypoTool )
DECLARE_COMPONENT( TrigMuisoHypoAlg )
DECLARE_COMPONENT( TrigMuisoHypoTool )
DECLARE_COMPONENT( TrigMuonEFTrackIsolationHypoAlg )
DECLARE_COMPONENT( TrigMuonEFTrackIsolationHypoTool )
DECLARE_COMPONENT( TrigMuonEFHypoAlg )
DECLARE_COMPONENT( TrigMuonEFHypoTool )
DECLARE_COMPONENT( TrigMuonEFInvMassHypoAlg )
DECLARE_COMPONENT( TrigMuonEFInvMassHypoTool )
DECLARE_COMPONENT( TrigL2MuonOverlapRemoverMufastAlg )
DECLARE_COMPONENT( TrigL2MuonOverlapRemoverMucombAlg )
DECLARE_COMPONENT( TrigL2MuonOverlapRemoverTool )
DECLARE_COMPONENT( TrigMuonLateMuRoIHypoAlg )
DECLARE_COMPONENT( TrigMuonLateMuRoIHypoTool )
